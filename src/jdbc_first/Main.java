package jdbc_first;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;



public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url,user,password);
			Statement statement = connexion.createStatement();
			
			String request = "SELECT * FROM personne";
			ResultSet result = statement.executeQuery(request);
			
			while(result.next()) {
				int idPersonne = result.getInt("num");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				
				System.out.println(nom + " " + prenom);
			}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}



package jdbc_second;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

public static void main(String[] args) throws ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			
			//On nomme le driver de la SGBD
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url,user,password);
			Statement statement = connexion.createStatement();
			
			String request = "INSERT INTO personne(nom,prenom) VALUES ('Wick','John')";
			int nbr = statement.executeUpdate(request,Statement.RETURN_GENERATED_KEYS);
			//0 correspond � pas de ligne ins�r�e et 1 correspond � success
			
			if(nbr !=0)
				System.out.println("insertion r�ussie");
			
						ResultSet resultat = statement.getGeneratedKeys();
			
			if(resultat.next()) {
				System.out.println("L'id g�n�r� pour cette personne est: " + resultat.getInt(1));
			}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

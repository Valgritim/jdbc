package jdbc_complet;

import java.util.*;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//Connexion � la base de donn�es:
		
		PersonneDaoImpl personneDao = new PersonneDaoImpl();
		
		System.out.println("--------------Insertion nouvelle personne------------");
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Entre le nom de la personne:");
		String nom = scan.nextLine();
		System.out.println("Entre le nom de la personne:");
		String prenom = scan.nextLine();
		
		Personne personne = new Personne(nom,prenom);
		personneDao.save(personne);
		
		List<Personne> listPersonnes = personneDao.getAll();
		
		for(Personne perso:listPersonnes) {
			
			System.out.println("Id:" + perso.getNum() + " " +"Nom:" + perso.getNom() + ", Prenom " + perso.getPrenom());
		}
		
		System.out.println("--------------Chercher une personne------------");
		
		System.out.println("Entre l'id de la personne � chercher:");
		int id = scan.nextInt();		
	
		Personne personneId = personneDao.findById(id);
		System.out.println(personneId);
		
		System.out.println("--------------Modifier une personne------------");
		
		System.out.println("Entre l'id de la personne � chercher:");
		int idModif = scan.nextInt();
		System.out.println("Entre un nouveau nom:");
		String newName = scan.next();
		System.out.println("Entre un nouveau pr�nom:");
		String newFirstname = scan.next();
		
		Personne newPerso = new Personne(idModif,newName,newFirstname);
		personneDao.update(newPerso);
		
		
		System.out.println("--------------Suppression d'une personne------------");
		
		System.out.println("Entre l'id de la personne � supprimer:");
		int idSup = scan.nextInt();	
		Personne personneASup = personneDao.remove(idSup);
		
		List<Personne> listPersonnes2 = personneDao.getAll();
		for(Personne perso:listPersonnes2) {
			
			System.out.println("Nom:" + perso.getNom() + ", Prenom " + perso.getPrenom());
		}
	}

}

package jdbc_third;

import java.sql.*;

public class Main {

public static void main(String[] args) throws ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			
			//On nomme le driver de la SGBD
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url,user,password);
			
			String request = "INSERT INTO personne(nom,prenom) VALUES (?,?)";
			PreparedStatement ps = connexion.prepareStatement(request,PreparedStatement.RETURN_GENERATED_KEYS);
							  ps.setString(1, "Wick");
							  ps.setString(2, "John");
							  ps.executeUpdate();
							  ResultSet resultat = ps.getGeneratedKeys();
					
			
			if(resultat.next())
							
				System.out.println("L'id g�n�r� pour cette personne est: " + resultat.getInt(1));
			
		}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

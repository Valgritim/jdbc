package jdbc_compl2;

public interface DAO <T> {
	void saveAndShow(T o);
	void removeAndShow(T o);
	void updateAndShow(T o);
}
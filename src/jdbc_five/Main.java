package jdbc_five;

import java.sql.*;


public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		

		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);

			PreparedStatement ps = connexion.prepareStatement("delete from personne where num = ? ");
			ps.setInt(1,7);
			int rows = ps.executeUpdate();
			
			if (rows == 1) {
				System.out.println("the line was deleted successfully!");
			} else if (rows ==0)
				System.out.println("the line is already deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
